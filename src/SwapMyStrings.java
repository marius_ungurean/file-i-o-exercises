import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SwapMyStrings {
    //input and output file names
    private Path input;

    //List of string to store file data
    private List<String> subStrings;

    //HashMap for storing words to swap as <key, value> pairs
    //initialised with desired pairs
    private Map<String, String> keywords = new HashMap<String, String>() {
        {
            put("Hello", "Privet");
            put("hello", "privet");
            put("one hundred", "100");
            put("one thousand", "1000");
            put("!", ".");

        }
    };

    //Constructor just with the input file name generating output file
    public SwapMyStrings(String input) {
        this.input = Paths.get(input);
        subStrings = new ArrayList<>();
    }

    //Splits the file lines into words and stores them into the subString list
    private void getFileData() {
        try {
            split(Files.lines(input).collect(Collectors.joining()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Splits the whole string into words (" " strings included after ",")
    // for an easy way to put the substrings back
    public void split(String str) {
        subStrings = Stream.of(str.split(","))
                .map(String::new)
                .collect(Collectors.toList());
    }

    //Finds if the list contains any key and swaps it with its value
    public void swapStrings(List<String> stringList) {
        for (int i = 0; i < stringList.size(); i++) {
            String s = stringList.get(i);
            System.out.println("String :" + s);
            for (String key : keywords.keySet()) {
                if (s.contains(key)) {
                    System.out.println("Key : " + key);
                    stringList.set(i, s.replace(key, keywords.get(key)));
                    System.out.println("New String :" + stringList.get(i));
                }
            }
        }

    }

    //Creates output file with given name and writes the modified strings to it
    public void writeOutput(String name) {
        getFileData();
        swapStrings(subStrings);
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : subStrings) {
            stringBuilder.append(s).append(",");
        }
        try {
            Files.write(Paths.get(name), Collections.singleton(stringBuilder.toString()), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.getMessage();
        }
    }
}
